import pytest
from dat_daemon_pyclient import DatDaemonClient


@pytest.fixture(scope='module', autouse=True)
def client():
    client = DatDaemonClient()
    yield client


def test_add_path(client):
    ret = client.add(path='./.yarnrc')
    assert 'exists' in ret['message'] or 'Added' in ret['message']


def test_add_key(client):
    ret = client.add(path='./', key='788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')
    assert 'exists' in ret['message']
    assert ret['failure'] == 2


def test_list(client):
    ret = client.list()
    assert next(x for x in ret['list'] if '788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507' in x['key'])


def test_info(client):
    ret = client.info('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')
    assert ret['key'] == '788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507'
    assert ret['statistics']['byteLength'] == 0


def test_stat(client):
    ret = client.stat('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507', './')
    keys = ['mode', 'uid', 'gid', 'size', 'blocks', 'offset', 'byteOffset', 'mtime', 'ctime']
    assert [x in ret['stat'].keys() for x in keys]


def test_readdir(client):
    ret = client.readdir('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')
    assert '.yarnrc' in ret['files']


@pytest.mark.asyncio
@pytest.mark.skip
async def test_create_write_stream(client):
    ret = await client.create_write_stream('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507', './.yarnrc')
    ret.close()
