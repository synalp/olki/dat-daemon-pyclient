PATH := $(HOME)/.poetry/bin:$(HOME)/.pyenv/bin:$(PATH)

build-protobuf:
	protoc --proto_path=./ --python_out=./dat_daemon_pyclient daemon.proto

test:
	@command -v poetry >/dev/null 2>&1 || curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python # if we lack poetry, we can install it
	#@command -v pyenv >/dev/null 2>&1 || curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
	poetry run pytest
